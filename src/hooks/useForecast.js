import {useState} from 'react';
import axios from 'axios'
import getCurrentDayForecast from '../helpers/getCurrentDayForecast';
import getCurrentDayDetailedForecast from '../helpers/getCurrentDayDetailedForecast';
import getUpcomingDaysForecast from '../helpers/getUpcomingDaysForecast';

const URL = 'https://api.openweathermap.org/data/2.5/weather?appid=23ea54865bddbce20e023a135435f90d&lang=es';
const forecastURL = 'api.openweathermap.org/data/2.5/forecast/daily?appid=23ea54865bddbce20e023a135435f90d&cnt=10';


const useForecast = () => {
    const [isError, setError] = useState(false);
    const [isLoading, setLoading] = useState(false);
    const [forecast, setForecast] = useState(false);

    const gatherForecastData = (data, location) => {
        data.main.temp = Math.trunc(data.main.temp - 273.15);
        data.main.temp_min = Math.trunc(data.main.temp_min - 273.15);
        data.main.temp_max = Math.trunc(data.main.temp_max - 273.15);
        console.log(data)
        const currentDay = getCurrentDayForecast(data, location);
        const currentDayDetails = getCurrentDayDetailedForecast(data);
        // getUpcomingDaysForecast(data)
        setForecast({currentDay, currentDayDetails});
        setLoading(false);

    }
    const submitRequest = async location => {
       try {
        setLoading(true);
        const { data } = await axios(URL, {params: {q : location}});
        const lon = data.coord.lon, lat = data.coord.lat;
        // futureDaysForecast(lon, lat);
        gatherForecastData(data, location);
        } catch (error){
            console.log(error)
            setLoading(false);
            setError('No hay información de la ciudad o esta no existe. Inténtelo de nuevo');
            return;
        }
    }
    // Aparentemente no funciona la api que saca el resto del pronóstico
    const futureDaysForecast = async (lon, lat) => {
        try {
            const{ futureData } = await axios (forecastURL, {params: {lat: lat, lon: lon}});
            console.log(futureData)
        } catch (error) {
            setError('Error en la base de datos')
        }
    }

    
    return {
        isError,
        isLoading,
        forecast,
        submitRequest,
    }
}

export default useForecast;