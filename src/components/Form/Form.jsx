import React, {useState} from 'react';
import PropTypes from 'prop-types';

import styles from './Form.module.css';

const Form = ({submitSearch}) => {
    const [location, setLocation] = useState('')
    const onSubmit = e => {
        e.preventDefault();
        if (!location || location === '')
            return;
        submitSearch(location);
    };
    return (
        <form onSubmit={onSubmit}>
            <input
                aria-label="location"
                type="text"
                className={`${styles.input} form-control my-2`}
                placeholder="Escriba una ciudad"
                vaule={location}
                required
                onChange={e => setLocation(e.target.value)}
            />

            <button type="submit" className={`${styles.button} col-md-6 offset-md-3 my-2 `} onClick={onSubmit}>
                Buscar
            </button>
        </form>
    );
};

Form.propTypes = {
    submitSearch: PropTypes.func.isRequired,
}

export default Form;
