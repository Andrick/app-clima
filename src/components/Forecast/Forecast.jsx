import React from 'react';
import PropTypes from 'prop-types';
import CurrentDay from '../CurrentDay'
import CurrentDayDescription from '../CurrentDayDescription';

import { Container, Row, Col } from 'react-bootstrap';

import styles from './Forecast.module.css';

const Forecast = ({forecast}) => (
        <Row className='px-5 row'>
            <Col md={6} className="px-5 d-flex">
                <div className={`my-auto ${styles.card}`}>
                    <CurrentDay {...forecast.currentDay}/>
                </div>
            </Col>
            <Col md={6} className="d-flex flex-column justify-content-between">
                <CurrentDayDescription {...forecast.currentDayDetails}></CurrentDayDescription>
            </Col>
        </Row>
);

Forecast.propTypes = {
    forecast: PropTypes.shape({
        currentDay: PropTypes.object,
        currentDayDetails: PropTypes.object,
    }),
}
export default Forecast;
