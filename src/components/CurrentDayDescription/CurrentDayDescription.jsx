import React from 'react';


const CurrentDayDescription = ({weatherIcon, weatherDescription, temperature, maxTemperature, minTemperature, pressure, humidity}) => {
    return(
    <div className="max-width text-capitalize">
        <div className={`d-flex flex-column mb-2`}>
            <div className="font-weigth-bold mb-1">
                <p className='my-5 font-weight-bolder text-uppercase'>Temperatura hoy:{`  ${temperature}`}°C</p>
                <p className='mb-0'> Temperatura máxima: {`  ${maxTemperature}`}°C</p>
                <p className='mb-0'>Temperatura mínima: {`  ${minTemperature}`} °C</p>
                <p className='mb-0'>Presión: {`  ${pressure}`} hPa</p>
                <p className='mb-0'>Humedad: {`  ${humidity}`} %</p>
            </div>
            <div className='justify-content-center'>
                <h2 className="font-weigth-bold my-5">
                    <img src={`http://openweathermap.org/img/w/${weatherIcon}.png`} alt={weatherDescription}></img>
                    <span>{weatherDescription}</span>
                </h2>
            </div>
            <div >
            </div>
        </div>
    </div>
    )
};
export default CurrentDayDescription;
