import React, { Fragment } from 'react';

import Header from '../Header';
import Form from '../Form';
import Error from '../Error';
import Loader from '../Loader';
import Forecast from '../Forecast';

import useForecast from '../../hooks/useForecast';

import styles from './Page.module.css'

const Page = () => {
    const {isError, isLoading, forecast, submitRequest} = useForecast();

    const onSubmit = value => {
        submitRequest(value)
    }
    return (
        <Fragment>
            <Header />
            <div className='container-fluid'>
                <div className={`${styles.box} row`}>
                    <div className='justify-content-center py-4 my-auto d-flex col-12'>
                        {!isLoading && <Form submitSearch={onSubmit} className="self-start"/>}
                    </div>
                    <div className='col-12 justify-content-center py-4'>
                        {!forecast && isError && <Error message={isError}></Error>}
                        {!forecast && isLoading && <Loader></Loader>}
                        {forecast && <Forecast forecast={forecast}></Forecast>}
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

export default Page;
