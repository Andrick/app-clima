const getCurrentDayDetailedForecast = (data) => ({
    temperature: Math.round(data.main.temp),
    maxTemperature: Math.round(data.main.temp_max),
    minTemperature: Math.round(data.main.temp_min),
    pressure: data.main.pressure,
    humidity: data.main.humidity,
    weatherIcon: data.weather[0].icon,
    weatherDescription: data.weather[0].description,
});

export default getCurrentDayDetailedForecast;
