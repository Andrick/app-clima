import moment from 'moment'
import 'moment/locale/es-mx';

moment.locale('es');
const getCurrentDayForecast = (data, title) => ({
    weekday: moment(data.applicable_date).format('dddd'),
    date: moment(data.applicable_date).format('MMMM Do'),
    location: title,
    temperature: Math.round(data.main.temp),
    weatherDescription: data.weather[0].description,
});

export default getCurrentDayForecast;
